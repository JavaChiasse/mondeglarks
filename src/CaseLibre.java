import java.util.ArrayList;
import java.util.List;

//import Direction.directionCardinale; WTF AHMED, T'AS TROP BU xD !


public class CaseLibre extends CaseJeu {
	
	/**
	 * La liste des glarks présents sur la case
	 */
	private List<Glark> listeDesGlarks;
	/**
	 * Indicateur de présence d'un tas de blurf
	 */
	private boolean blurf;
	
	//Le damier doit être bien initialisé.
	private Damier damier; //Permet d'accéder au damier
	
	// CaseLibre( randomX,randomY,unJeu.getDamier(),unJeu.getListeDesGlarks() )
	/**
	 * Retourne une instance de CaseLibre
	 * @param unePositionX
	 * @param unePositionY
	 * @param unDamier
	 */
	public CaseLibre(int unePositionX, int unePositionY, Damier unDamier) {
		super(unePositionX, unePositionY);
		setDamier( unDamier );
		blurf = false;
		setListeDesGlarks(new ArrayList<Glark>());
	}
	/**
	 * Ajoute un glark sur la case "libre"
	 * @param unNouveauGlark
	 */
	public void addGlark(Glark unNouveauGlark) {
		getListeDesGlarks().add(unNouveauGlark);
	}
	
	//Supprime un glark de la case, car celui-ci s'est déplacé ou est décédé.
	/**
	 * Supprime un glark de la case "libre"
	 * @param glarkASupprimer
	 */
	public void supprimeGlark(Glark glarkASupprimer) {
		getListeDesGlarks().remove(getListeDesGlarks().indexOf(glarkASupprimer));
	}
		
	
	//Renvoie un entier supérieur à 0 si la classe contient un tas de blurf.
	@Override
	boolean asTuDuBlurf(){
		return blurf;
	}
	
	//Supprime le tas de blurf de la case, après qu'il ait été mangé par exemple.
	void supprimeTasDeBlurf(){
		blurf=false;
	}
	
	//On accède aux cases voisines de celle-ci.
	//Le paramètre c doit représenter le Sud, l'Ouest, l'Est et le Nord par les caractères S, O, E ou N, respectivement.
	CaseJeu accedeVoisine(char directionCardinale){
		switch(directionCardinale){
		case 'S' : return donneLaCaseSud();
		case 'N' : return donneLaCaseNord();
		case 'O' : return donneLaCaseOuest();
		case 'E' : return donneLaCaseEst();
		default : return this;
		}
	}
	
	private CaseJeu donneLaCaseOuest(){
		return getDamier().donneTaCase( getPosX()-1,getPosY() );
	}
	
	private CaseJeu donneLaCaseEst(){
		return getDamier().donneTaCase( getPosX()+1,getPosY() );
	}
	
	private CaseJeu donneLaCaseNord(){
		return getDamier().donneTaCase( getPosX(),getPosY()+1 );
	}
	
	private CaseJeu donneLaCaseSud(){
		return getDamier().donneTaCase( getPosX(),getPosY()-1 );
	}
	
	//Offre au glark passé en paramètre de manger le blurf (si il y en a ), de manger les glarks présents (s'il y en a ).
	//Remarque: C'est le Glark qui sait ce qu'il aime manger; la case elle, propose démocratiquement le même menu à tout le monde.
	public void menu(Glark unGlark){
		boolean glarkWasEaten=false;
		for(Glark tamponGlark : getListeDesGlarks() ){
			glarkWasEaten = unGlark.devoreGlark(tamponGlark);
			if ( glarkWasEaten )
				supprimeGlark(tamponGlark);
		}
		boolean blurfPileWasEaten=false;
		blurfPileWasEaten = unGlark.devoreBlurf();
		if ( blurfPileWasEaten ) supprimeTasDeBlurf();
	}

	public Damier getDamier() {
		return damier;
	}

	public void setDamier(Damier damier) {
		this.damier = damier;
	}

	public List<Glark> getListeDesGlarks() {
		return listeDesGlarks;
	}

	public void setListeDesGlarks(List<Glark> listeDesGlarks) {
		this.listeDesGlarks = listeDesGlarks;
	}
	
}
