public class Direction {
	
	/**
	 * 
	 * Type énuméré représentant les directions cardinales.
	 *
	 */
	public static enum directionCardinale {
		NORD, SUD, OUEST, EST;
	}
	
	/**
	 * La direction qu'avait le glark auparavant.
	 */
	private directionCardinale sens;
	
	/**
	 * Tampon utilisé pour mémoriser un nouveau sens.
	 */
	private directionCardinale sensDeplacement;

	/**
	 * Constructeur de la classe Direction.
	 * @param sensOrigine
	 * Le sens du glark avant changement possible de direction.
	 */
	public Direction(directionCardinale sensOrigine) {
		sens = sensOrigine;
		sensDeplacement = sensOrigine;
	}

	/**
	 * Détermine quelle case est à l'ouest de la case passée en paramètre, et la renvoie.
	 * @param uneCase
	 * Les calcul de voisins seront relatifs à cette case.
	 * @return
	 * Une CaseJeu voisine de uneCase à l'ouest.
	 */
	private CaseJeu caseOuest(CaseLibre uneCase) {
		sensDeplacement = directionCardinale.OUEST;
		return uneCase.getDamier().donneTaCase(uneCase.getPosX() - 1, uneCase.getPosY());
	}

	/**
	 * Détermine quelle case est à l'est de la case passée en paramètre, et la renvoie.
	 * @param uneCase
	 * Les calcul de voisins seront relatifs à cette case.
	 * @return
	 * Une CaseJeu voisine de uneCase à l'est.
	 */
	private CaseJeu caseEst(CaseLibre uneCase) {
		sensDeplacement = directionCardinale.EST;
		return uneCase.getDamier().donneTaCase(uneCase.getPosX() + 1, uneCase.getPosY());
	}

	/**
	 * Détermine quelle case est au nord de la case passée en paramètre, et la renvoie.
	 * @param uneCase
	 * Les calcul de voisins seront relatifs à cette case.
	 * @return
	 * Une CaseJeu voisine de uneCase au nord.
	 */
	private CaseJeu caseNord(CaseLibre uneCase) {
		sensDeplacement = directionCardinale.NORD;
		return uneCase.getDamier().donneTaCase(uneCase.getPosX(), uneCase.getPosY() + 1);
	}

	/**
	 * Détermine quelle case est au sud de la case passée en paramètre, et la renvoie.
	 * @param uneCase
	 * Les calcul de voisins seront relatifs à cette case.
	 * @return
	 * Une CaseJeu voisine de uneCase au sud.
	 */
	private CaseJeu caseSud(CaseLibre uneCase) {
		sensDeplacement = directionCardinale.SUD;
		return uneCase.getDamier().donneTaCase(uneCase.getPosX(), uneCase.getPosY() - 1);
	}

	/**
	 * Détermine quelle case est en face de la case passée en paramètre selon la direction du glark, et la renvoie.
	 * @param uneCase
	 * Les calcul de voisins seront relatifs à cette case.
	 * @return
	 * Une CaseJeu en face de uneCase.
	 */
	public CaseJeu voisineDansDir(CaseLibre uneCase) {
		if (sens == directionCardinale.NORD) {

			return caseNord(uneCase);

		} else if (sens == directionCardinale.SUD) {
			return caseSud(uneCase);

		} else if (sens == directionCardinale.OUEST) {

			return caseOuest(uneCase);

		} else if (sens == directionCardinale.EST) {

			return caseEst(uneCase);

		} else {
			return uneCase;
		}

	}

	/**
	 * Détermine quelle case est à gauche de la case passée en paramètre selon la direction du glark, et la renvoie.
	 * @param uneCase
	 * Les calcul de voisins seront relatifs à cette case.
	 * @return
	 * Une CaseJeu à gauche de uneCase.
	 */
	public CaseJeu caseGauche(CaseLibre uneCase) {
		if (sens == directionCardinale.NORD) {

			return caseOuest(uneCase);

		} else if (sens == directionCardinale.SUD) {
			return caseEst(uneCase);

		} else if (sens == directionCardinale.OUEST) {

			return caseSud(uneCase);

		} else if (sens == directionCardinale.EST) {

			return caseNord(uneCase);

		} else {
			return uneCase;
		}

	}

	/**
	 * Détermine quelle case est à droite de la case passée en paramètre selon la direction du glark, et la renvoie.
	 * @param uneCase
	 * Les calcul de voisins seront relatifs à cette case.
	 * @return
	 * Une CaseJeu à droite de uneCase.
	 */
	public CaseJeu caseDroit(CaseLibre uneCase) {
		if (sens == directionCardinale.NORD) {

			return caseEst(uneCase);

		} else if (sens == directionCardinale.SUD) {
			return caseOuest(uneCase);

		} else if (sens == directionCardinale.OUEST) {

			return caseNord(uneCase);

		} else if (sens == directionCardinale.EST) {

			return caseSud(uneCase);

		} else {
			return uneCase;
		}

	}

	/**
	 * Détermine quelle case est à l'arrière de la case passée en paramètre selon la direction du glark, et la renvoie.
	 * @param uneCase
	 * Les calcul de voisins seront relatifs à cette case.
	 * @return
	 * Une CaseJeu à l'arrière de uneCase.
	 */
	public CaseJeu caseOpposee(CaseLibre uneCase) {
		if (sens == directionCardinale.NORD) {

			return caseSud(uneCase);

		} else if (sens == directionCardinale.SUD) {
			return caseNord(uneCase);

		} else if (sens == directionCardinale.OUEST) {

			return caseEst(uneCase);

		} else if (sens == directionCardinale.EST) {

			return caseOuest(uneCase);

		} else {
			return uneCase;
		}

	}


	/**
	 * Donne la direction cardinale du glark
	 * @return
	 * direction cardinale du glark
	 */
	public directionCardinale getSens() {
		return sens;
	}

	/**
	 * Redéfinit la direction cardinale du glark
	 * @param sens
	 * direction cardinale du glark
	 */
	public void setSens(directionCardinale sens) {
		this.sens = sens;
	}

	/**
	 * Donne le tampon de la direction cardinale du glark, représente la nouvelle direction.
	 * @return
	 * direction cardinale du glark
	 */
	public directionCardinale getSensDeplacement() {
		return sensDeplacement;
	}

	/**
	 * Redéfinit le tampon de la direction cardinale du glark
	 * @param sens
	 * direction cardinale du glark
	 */
	public void setSensDeplacement(directionCardinale sensDeplacement) {
		this.sensDeplacement = sensDeplacement;
	}

}
