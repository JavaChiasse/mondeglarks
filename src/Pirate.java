import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 
 * Comme les born�s et les gloutons,ils avancent d'une case � chaque cycle.Ils sont capables de percevoir ce qui se passe � une case de leur position.
 * Ils ne sont pas capables de manger les tas de blurf directement ( lorsqu'ils vont sur une case o� se trouve un tas de blurf il ne se passe rien ) ,
 * mais ils peuvent r�cup�rer les points d'�nergie des autres Glarks ( et en particulier des autres pirates aussi) en les d�vorant,
 * ce qui �limine ces derniers du jeu. 
 * S'ils per�oivent un (ou plusieurs) glarks sur une case voisine, ils se dirigent donc vers cette case pour les d�vorer.
 *
 */
public class Pirate extends Glark {
	/**
	 * 
	 * Constructeur cr�ant un Pirate .
	 * @param uneEnergie
	 * L'�nergie de d�part du Glark.
	 * @param uneCaseCourante
	 * La position de d�part du Glark.
	 */
	public Pirate(int uneEnergie, CaseLibre uneCaseCourante) {
		super(uneEnergie, uneCaseCourante);
	}
	/**
	 * Les pirates ne mangent pas de blurf.
	 */
	@Override
	public boolean devoreBlurf() {
		return false;
	}
	/**
	 * Vole l'�nergie des glarks de la case courante en les tuant.
	 */
	public boolean devoreGlark(Glark unGlarkRepas){
		addEnergie(unGlarkRepas.getEnergie());
		return true;
	}
	/**
	 * Le pirate v�rifie les cases voisines orthogonalement et se dirige vers une case o� des glarks se trouvent, en priorit�.
	 * Si il ne trouve pas de case occup�e par des glarks, alors il erre au hasard.
	 */
	public CaseLibre trouveDestination() {
		
		boolean glarksOuest = false;
		boolean glarksEst = false;
		boolean glarksNord = false;
		boolean glarksSud = false;
		
		CaseLibre tampon;
		
		if ( caseOuest().estObstacle() == false ) {
			tampon = (CaseLibre) caseOuest();
			glarksOuest = tampon.getListeDesGlarks().size() > 0 ;
		}
		if ( caseEst().estObstacle() == false ) {
			tampon = (CaseLibre) caseEst();
			glarksEst = tampon.getListeDesGlarks().size() > 0 ;
		}
		if ( caseNord().estObstacle() == false ) {
			tampon = (CaseLibre) caseNord();
			glarksNord = tampon.getListeDesGlarks().size() > 0 ;
		}
		if ( caseSud().estObstacle() == false ) {
			tampon = (CaseLibre) caseSud();
			glarksSud = tampon.getListeDesGlarks().size() > 0 ;
		}
		
		
		if ( glarksOuest ){
			return (CaseLibre) caseOuest();
		}else if ( glarksEst ){
			return (CaseLibre) caseEst();
		}else if ( glarksNord ){
			return (CaseLibre) caseNord();
		}else if ( glarksSud ){
			return (CaseLibre) caseSud();
		}else{
			List<CaseLibre> listeCasesLibres= new ArrayList<CaseLibre>();
			
			
			if ( caseOuest().estObstacle() == false ) listeCasesLibres.add( (CaseLibre) caseOuest() );
			if ( caseEst().estObstacle() == false ) listeCasesLibres.add( (CaseLibre) caseEst() );
			if ( caseNord().estObstacle() == false ) listeCasesLibres.add( (CaseLibre) caseNord() );
			if ( caseSud().estObstacle() == false ) listeCasesLibres.add( (CaseLibre) caseSud() );
			
			Random generateurAleatoire = new Random();
			int nombreAleatoire = generateurAleatoire.nextInt( listeCasesLibres.size() );
			
			return listeCasesLibres.get(nombreAleatoire);
		}
	}
	/**
	 * 	Renvoie la case � l'ouest de la case courante
	 * @return
	 * la case � l'ouest de la case courante
	 */
	private CaseJeu caseOuest(){
		return caseCourante.getDamier().donneTaCase( caseCourante.getPosX()-1,caseCourante.getPosY() );
	}
	/**
	 * 	Renvoie la case � l'est de la case courante
	 * @return
	 * la case � l'est de la case courante
	 */
	private CaseJeu caseEst(){
		return caseCourante.getDamier().donneTaCase( caseCourante.getPosX()+1,caseCourante.getPosY() );
	}
	/**
	 * 	Renvoie la case au nord de la case courante
	 * @return
	 * la case � au nord de la case courante
	 */
	private CaseJeu caseNord(){
		return caseCourante.getDamier().donneTaCase( caseCourante.getPosX(),caseCourante.getPosY()+1 );
	}
	/**
	 * 	Renvoie la case au sud de la case courante
	 * @return
	 * la case au sud de la case courante
	 */
	private CaseJeu caseSud(){
		return caseCourante.getDamier().donneTaCase( caseCourante.getPosX(),caseCourante.getPosY()-1 );
	}
	
}
