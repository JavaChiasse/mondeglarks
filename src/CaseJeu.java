public abstract class CaseJeu {
	
	private int posX, posY;
	
	/**
	 * Retourne une instance de CaseJeu, représente une case du damier
	 * @param unePositionX La position X de la case
	 * @param unePositionY La position Y de la case
	 */
	public CaseJeu(int unePositionX, int unePositionY) {
		setPosX(unePositionX);
		setPosY(unePositionY);
	}
	/**
	 * Vérifie si la case est un obstacle
	 * @return
	 */
	public boolean estObstacle() {
		return false;
	}	
	
	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}
	
	/**
	 * Vérifie si la case dispose de Blurf
	 * @return boolean
	 */
	boolean asTuDuBlurf() {
		return false;
	}

}
