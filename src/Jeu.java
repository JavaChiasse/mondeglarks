import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Jeu {
	
	/**
	 * Les cases sont mémorisées dans damier
	 */
	private static Damier damier;
	
	/**
	 * Les Glarks morts ou vifs sont mémorisés dans listeDesGlarks
	 */
	private static List<Glark> listeDesGlarks;
	
	private int nombreGlark, tailleX, tailleY;
	
	/**
	 * Retourne une instance de Jeu (Monde des Glarks)
	 * @param uneTailleX Taille du plateau X
	 * @param uneTailleY Taille du plateau Y
	 * @param unNombreGlark Nombre de Glark à générer
	 * @param unNombreObstacle Nombre d'obstacle à poser
	 */
	public Jeu(int uneTailleX, int uneTailleY, int unNombreGlark, int unNombreObstacle) {
		nombreGlark = unNombreGlark;
		tailleX = uneTailleX;
		tailleY = uneTailleY;
	}
	
	/**
	 * 
	 * @return int Nombre de glark encore en vie
	 */
	public int glarkEnVie() {
		int nVie = 0;
		for (Glark unGlark : listeDesGlarks) {
			if (unGlark.getEnergie() > 0) {
				nVie++;
			}
		}
		
		return nVie;
	}
	
	/**
	 * Initialise le plateau et place les objets sur le damier
	 */
	public void initialiseToi(){
		listeDesGlarks = new ArrayList<Glark>();
		damier = new Damier(tailleX, tailleY);
		Random generateurAleatoire = new Random();
		int nombreAléatoire = 0;
		
		CaseLibre uneCaseTemp;
		
		//On place les Glarks (Glouton, Borné, Pirate)
		for(int i = 0; i < tailleX; i++) {
			for (int j = 0; j < tailleY; j++) {
						
				nombreAléatoire = generateurAleatoire.nextInt(20);
				uneCaseTemp = new CaseLibre(i, j, damier);
				damier.modifieTaCase(i, j, uneCaseTemp);
						
				if (nombreAléatoire > 15) {
					
					uneCaseTemp.addGlark(new Glouton(100, uneCaseTemp));
							
				}else if(nombreAléatoire > 13) {
							
					uneCaseTemp.addGlark(new Pirate(100, uneCaseTemp));
							
				}else if(nombreAléatoire > 10){
							
					uneCaseTemp.addGlark(new Borne(100, uneCaseTemp));
							
				}
			}
		}
		
		//On place les obstacles
		for(int i = 0; i < tailleX; i++) {
			for (int j = 0; j < tailleY; j++) {
				if (generateurAleatoire.nextInt(10) > 5) {
					damier.modifieTaCase(i, j, new CaseObstacle(i, j));
				}
			}
		}
		
	}
	
	/**
	 * Tant qu'il y a un Glark en vie, chaque Glark en vie reçoit le message vasY.
	 */
	public void lanceToi(){
		while (glarkEnVie() > 0) {
			for (Glark unGlark : listeDesGlarks) {
				unGlark.vasY();
			}
		}
	}
	
	//Tant qu'il y a un Glark en vie, chaque Glark en vie reçoit le message vasY.
		public void lanceToi(boolean flagAffichage){
			while (glarkEnVie() > 0) {
				if (flagAffichage) Affiche();
				for (Glark unGlark : listeDesGlarks) {
					unGlark.vasY();
				}
			}
		}
	
	public void Affiche() {
			//Affichage en début de tour.	
		}
	
	/**
	 * 
	 * @return List<Glark> Liste des glarks sur le damier
	 */
	public static List<Glark> getListeDesGlarks(){
		return listeDesGlarks;
	}
	
	public static Damier getDamier(){
		return damier;
	}

}
