import java.util.Random;

/**
 * 
 * A chaque cycle, un Borné avance d'une case dans une direction choisie initialement de manière aléatoire. 
 * S'il rencontre un obstacle, c'est-à-dire s'il perçoit un obstacle se trouvant devant lui, il change de direction,
 *  en allant vers la gauche ou la droite. 
 * Si les cases à gauche et à droite sont aussi des obstacles, il fait demi-tour et rebrousse chemin.
 * 
 * Lorsqu'il arrive sur une autre case, un borné dévore les tas de blurf et tous les Glarks qui s'y trouvent sauf les Bornés. 
 * Lorsqu'il dévore un Glark il gagne 10 points d'énergie ( indépendamment de l'énergie de ce Glark ). 
 * Un Glark dévoré est supprimé du jeu.
 *
 */
public class Borne extends Glark {

	/**
	 * 
	 * Constructeur créant un Borné ayant une direction initiale aléatoirement choisie.
	 * @param uneEnergie
	 * L'énergie de départ du Glark.
	 * @param uneCaseCourante
	 * La position de départ du Glark.
	 */
	
	//Indique dans quelle direction se déplace le borné si le champ est libre
		Direction direction;
		
	public Borne(int uneEnergie, CaseLibre uneCaseCourante) {
		super(uneEnergie, uneCaseCourante);
		
		Random generateurAleatoire = new Random();
		int nombreAléatoire = generateurAleatoire.nextInt(4)+1;
		switch(nombreAléatoire){
		case 1 : direction = new Direction(Direction.directionCardinale.NORD);
		case 2 : direction = new Direction(Direction.directionCardinale.OUEST);
		case 3 : direction = new Direction(Direction.directionCardinale.EST);
		case 4 : direction = new Direction(Direction.directionCardinale.SUD);
		default : direction = new Direction(Direction.directionCardinale.NORD);
		}
		
		// TODO Auto-generated constructor stub
	}

	
	
	/**
	 * Méthode qui demande au Glark s'il est borné ou non.
	 */
	@Override
	public boolean estBorne(){
		return true;
	}
	
	/**
	 * Dévore les Glark proposés si ils ne sont pas Bornés.
	 * Chaque repas fait augmenter de 10 l'énergie du mangeur.
	 */
	public boolean devoreGlark(Glark unGlarkRepas){
		if ( unGlarkRepas.estBorne() == false ){
			addEnergie(10);
			return true;
		}
		return false;
	}
	
	/**
	 * Si la case faisant face au Borné est libre( pas un obstacle ), renvoie vrai, sinon, renvoie faux.
	 * @return
	 * true ou false selon si la case est libre ou non.
	 */
	private boolean caseDevantEstLibre(){
		return direction.voisineDansDir(getCaseCourante()).estObstacle() == false;
	}
	
	/**
	 * Renvoie la case cible du prochain déplacement du Borné
	 * si la case faisant face au Borné est libre, renvoie cette case.
	 * sinon direction = gauche, si gauche est libre return cette case
	 * sinon direction = droite, si droite est libre return cette case
	 * sinon direction = opposée, si opposée est libre return cette case.
	 */
	public CaseLibre trouveDestination() {
		
		if ( caseDevantEstLibre() ){
			//si la case faisant face au Borné est libre, renvoie cette case.
			return (CaseLibre) direction.voisineDansDir(getCaseCourante());
			
		}else if( direction.caseGauche(getCaseCourante()).estObstacle() == false ){
			//sinon direction = gauche, si gauche est libre return cette case
			direction=new Direction( direction.getSensDeplacement() );
			
			return (CaseLibre) direction.caseGauche(getCaseCourante());
			
		}else if( direction.caseDroit(getCaseCourante()).estObstacle() == false ){
			//sinon direction = droite, si droite est libre return cette case
			return (CaseLibre) direction.caseGauche(getCaseCourante());
			
		}else if( direction.caseGauche(getCaseCourante()).estObstacle() == false ){
			//sinon direction = opposée, si opposée est libre return cette case.
			return (CaseLibre) direction.caseGauche(getCaseCourante());
		}
		return null;
	}

}
