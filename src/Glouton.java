import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 
 * Comme les bornés, les gloutons avancent d'une case à chaque cycle.
 * Mais à la différence des born�s, ils sont capables de percevoir les tas de blurf à une case de distance dans les quatre directions 
 * ( haut, bas, gauche, droite).
 * Ils peuvent aussi se diriger vers eux.
 * S'ils ne percoivent rien, ils se déplacent en choisissant une direction de manière aléatoire.
 *
 */
public class Glouton extends Glark {
	
	/**
	 * 
	 * Constructeur créant un Glouton .
	 * @param uneEnergie
	 * L'énergie de départ du Glark.
	 * @param uneCaseCourante
	 * La position de départ du Glark.
	 */
	public Glouton(int uneEnergie, CaseLibre uneCaseCourante) {
		super(uneEnergie, uneCaseCourante);	
	}
	
	/**
	 * 
	 */
	public CaseLibre trouveDestination(){
		//chercher blurf, sinon return premi�re case libre.
		boolean blurfOuest = caseOuest().asTuDuBlurf();
		boolean blurfEst = caseEst().asTuDuBlurf();
		boolean blurfNord = caseNord().asTuDuBlurf();
		boolean blurfSud = caseSud().asTuDuBlurf();
		
		if ( blurfOuest ){
			return (CaseLibre) caseOuest();
		}else if ( blurfEst ){
			return (CaseLibre) caseEst();
		}else if ( blurfNord ){
			return (CaseLibre) caseNord();
		}else if ( blurfSud ){
			return (CaseLibre) caseSud();
		}else{
			
			List<CaseLibre> listeCasesLibres= new ArrayList<CaseLibre>();
			
			
			if ( caseOuest().estObstacle() == false ) listeCasesLibres.add( (CaseLibre) caseOuest() );
			if ( caseEst().estObstacle() == false ) listeCasesLibres.add( (CaseLibre) caseEst() );
			if ( caseNord().estObstacle() == false ) listeCasesLibres.add( (CaseLibre) caseNord() );
			if ( caseSud().estObstacle() == false ) listeCasesLibres.add( (CaseLibre) caseSud() );
			
			Random generateurAleatoire = new Random();
			int nombreAleatoire = generateurAleatoire.nextInt( listeCasesLibres.size() );
			
			return listeCasesLibres.get(nombreAleatoire);
		}
	}
	
	
	/**
	 * 	Renvoie la case à l'ouest de la case courante
	 * @return
	 * la case à l'ouest de la case courante
	 */
	private CaseJeu caseOuest(){
		return caseCourante.getDamier().donneTaCase( caseCourante.getPosX()-1,caseCourante.getPosY() );
	}
	
	/**
	 * 	Renvoie la case à l'est de la case courante
	 * @return
	 * la case à l'est de la case courante
	 */
	private CaseJeu caseEst(){
		return caseCourante.getDamier().donneTaCase( caseCourante.getPosX()+1,caseCourante.getPosY() );
	}
	
	/**
	 * 	Renvoie la case au nord de la case courante
	 * @return
	 * la case à au nord de la case courante
	 */
	private CaseJeu caseNord(){
		return caseCourante.getDamier().donneTaCase( caseCourante.getPosX(),caseCourante.getPosY()+1 );
	}
	
	/**
	 * 	Renvoie la case au sud de la case courante
	 * @return
	 * la case au sud de la case courante
	 */
	private CaseJeu caseSud(){
		return caseCourante.getDamier().donneTaCase( caseCourante.getPosX(),caseCourante.getPosY()-1 );
	}
	
}
