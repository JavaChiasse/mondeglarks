import java.util.ArrayList;
import java.util.List;

public class Damier {
	
	/**
	 * Liste des cases du damier
	 */
	private List<CaseJeu> listeCases;
	
	private int tailleX, tailleY;
	
	/**
	 * Retourne une instance de Damier
	 * @param uneTailleX Taille du damier sur axe X
	 * @param uneTailleY Taille du damier sur axe Y
	 */
	public Damier(int uneTailleX, int uneTailleY) {
		listeCases = new ArrayList<CaseJeu>();
		tailleX = uneTailleX;
		tailleY = uneTailleY;
	}

	/**
	 * Récupère la case sur le damier à la coordonnée X,Y
	 * @param i Position X
	 * @param j Position Y
	 * @return CaseJeu 
	 */
	public CaseJeu donneTaCase(int i,int j){
		return listeCases.get(i*j+1);
	}
	
	/**
	 * Modifie une case étant dans le damier
	 * @param i Position X
	 * @param j Position Y
	 * @param uneCase
	 */
	public void modifieTaCase(int i, int j, CaseJeu uneCase){
		if (i*j < listeCases.size()) {
			listeCases.set(i*j, uneCase);
		}
	}
	/**
	 * Récupère la taille en largeur du damier
	 * @return int
	 */
	public int getTailleX() {
		return this.tailleX;
	}
	/**
	 * Récupère la taille en hauteur du damier
	 * @return int
	 */
	public int getTailleY() {
		return this.tailleY;
	}
	

}
