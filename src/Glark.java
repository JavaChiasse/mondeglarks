public abstract class Glark {
	
	/**
	 * Énergie du Glark actuel
	 */
	private int energie;
	/**
	 * La case sur lequel le Glark est situé
	 */
	protected CaseLibre caseCourante;
	
	/**
	 * Retourne une instance de Glark
	 * @param uneEnergie Une énergie initiale
	 * @param uneCaseCourante Une case courante initiale
	 */
	public Glark(int uneEnergie, CaseLibre uneCaseCourante) {
		setEnergie(uneEnergie);
		setCaseCourante(uneCaseCourante);
	}

	/**
	 * @return the energie
	 */
	public int getEnergie() {
		return energie;
	}

	/**
	 * @param energie the energie to set
	 */
	public void setEnergie(int energie) {
		this.energie = energie;
	}
	/**
	 * Ajoute une quantité d'énergie au Glark
	 * @param uneQteSupp La quantité cible
	 */
	public void addEnergie(int uneQteSupp) {
		this.energie += uneQteSupp;
	}
	/**
	 * Retire une quantité d'énergie au Glark
	 * @param uneQte La quantité cible
	 */
	public void prendEnergie(int uneQte) {
		this.energie -= uneQte;
	}

	/**
	 * @return the caseCourante
	 */
	public CaseLibre getCaseCourante() {
		return caseCourante;
	}

	/**
	 * @param caseCourante the caseCourante to set
	 */
	public void setCaseCourante(CaseLibre laCaseCourante) {
		caseCourante = laCaseCourante;
	}
	/**
	 * Retire toute l'énergie du Glark et met fin à sa vie
	 * @return boolean
	 */
	public boolean meurs() {
		if (this.energie == 0) {
			return false;
		}else{
			this.energie = 0;
			caseCourante.supprimeGlark(this);
			return true;
		}
	}
	/**
	 * Vérifie si le glark actuel est bornée
	 * @return boolean
	 */
	public boolean estBorne() {
		return false;
	}
	/**
	 * Mange le tas de blurf et acquiere 100 points d'énergie
	 * @return boolean
	 */
	public boolean devoreBlurf() {
		
		if (caseCourante.asTuDuBlurf()) {
			addEnergie(100);
			caseCourante.supprimeTasDeBlurf();
			return true;
		}
		
		return false;
	}

	/**
	 * Dévore un Glark
	 * @param tamponGlark Le glark cible
	 * @return boolean
	 */
	public boolean devoreGlark(Glark tamponGlark) {
		return false;
	}
	/**
	 * Trouve la destination du Glark actuel
	 * @return
	 */
	public CaseLibre trouveDestination(){
		return caseCourante;
	}
	
	/**
	 * message envoyé par le jeu qui provoque diminution de l'énergie nécessaire au cycle, si le Glark est épuisé, il meurt, sinon il recherche sa destination
	 * si la case retournée est différente de la case courante, il s'y déplace, et elle lui présente son menu
	 */
	public void vasY(){
		prendEnergie(1);
		if ( getEnergie() <= 0 ) caseCourante.supprimeGlark(this);
		setCaseCourante( trouveDestination() );
	}

}
